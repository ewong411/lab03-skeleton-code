package course.labs.activitylab

import android.os.Bundle
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_one.*

class ActivityOne : Activity() {

    // lifecycle counts
    //TODO: Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
    //TODO:  increment the variables' values when their corresponding lifecycle methods get called and log the info
    private lateinit var vCreate: TextView
    private lateinit var vStart: TextView
    private lateinit var vResume: TextView
    private lateinit var vPause: TextView
    private lateinit var vStop: TextView
    private lateinit var vRestart: TextView
    private lateinit var vDestroy: TextView

    internal var createCounter:Int = 0
    internal var startCounter:Int = 0
    internal var resumeCounter:Int = 0
    internal var pauseCounter:Int = 0
    internal var stopCounter:Int = 0
    internal var restartCounter:Int = 0
    internal var destroyCounter:Int = 0




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)

        //placing the id in a variable
        vCreate = findViewById(R.id.create) as TextView

        //Log cat print out
        Log.i(TAG, "onCreate called")

        // only one shared prefs file, so name defaults
        val prefs = getPreferences(Context.MODE_PRIVATE)

        //using the preferences
        createCounter = prefs!!.getInt("createCounter", 0)
        startCounter = prefs!!.getInt("startCounter",0)
        resumeCounter = prefs!!.getInt("resumeCounter",0)
        pauseCounter = prefs!!.getInt("pauseCounter",0)
        stopCounter = prefs!!.getInt("stopCounter",0)
        restartCounter = prefs!!.getInt("restartCounter",0)
        destroyCounter = prefs!!.getInt("destroyCounter",0)

        //setting the display for the UI
        if(createCounter !== 0){
            (findViewById(R.id.create) as TextView).text = "onCreate() calls: " + createCounter.toString()
            (findViewById(R.id.start) as TextView).text = "onStart() calls: " + startCounter.toString()
            (findViewById(R.id.resume) as TextView).text = "onResume() calls: " + resumeCounter.toString()
            (findViewById(R.id.pause) as TextView).text = "onPause() calls: " + pauseCounter.toString()
            (findViewById(R.id.stop) as TextView).text = "onStop() calls: " + stopCounter.toString()
            (findViewById(R.id.restart) as TextView).text = "onRestart() calls: " + restartCounter.toString()
            (findViewById(R.id.destroy) as TextView).text = "onDestroy() calls: " + destroyCounter.toString()
        }

        Log.i(TAG, "restored counterCreate" + createCounter)
        //check whether we're recreating a previous instance
       /* if (savedInstanceState != null){
            with(savedInstanceState){
                createCounter = getInt("createCounter")
                startCounter = getInt("startCounter")
                resumeCounter = getInt("resumeCounter")
                pauseCounter = getInt("pauseCounter")
                stopCounter = getInt("stopCounter")
                restartCounter = getInt("restartCounter")
                destroyCounter = getInt("destroyCounter")
            }
        }else{
            createCounter = 0
            startCounter = 0
            resumeCounter = 0
            pauseCounter = 0
            stopCounter = 0
            restartCounter = 0
            destroyCounter = 0
        }*/


        //TODO: update the appropriate count variable & update the view
        createCounter+=1
        vCreate.text = "onCreate() calls: " + createCounter.toString()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_one, menu)
        return true
    }

    // lifecycle callback overrides

    public override fun onStart() {
        super.onStart()

        //Log cat print out
        Log.i(TAG, "onStart called")
        vStart = findViewById(R.id.start) as TextView
        //TODO:  update the appropriate count variable & update the view
        startCounter+=1
        vStart.text = "onStart() calls:" + startCounter.toString()

    }

    public override fun onPause() {
        super.onPause()
        //Log cat print out
        Log.i(TAG, "onPause called")


        vPause = findViewById(R.id.pause) as TextView
        pauseCounter+=1
        vPause.text = "onPause() calls:" + pauseCounter.toString()
    }

    public override fun onRestart() {
        super.onRestart()
        //Log cat print out
        Log.i(TAG, "onRestart called")
        vRestart = findViewById(R.id.restart) as TextView
        restartCounter+=1
        vRestart.text = "onRestart() calls:" + restartCounter.toString()
    }

    public override fun onDestroy() {
        super.onDestroy()
        //Log cat print out
        Log.i(TAG, "onDestroy Called")
        vDestroy = findViewById(R.id.destroy) as TextView
        destroyCounter+=1
        vDestroy.text = "onDestroy() calls:" + destroyCounter.toString()

    }

    public override fun onResume() {
        super.onResume()
        //Log cat print out
        Log.i(TAG,"onResume called")
        vResume = findViewById(R.id.resume) as TextView
        resumeCounter+=1
        vResume.text = "onResume() calls:" + resumeCounter.toString()
    }
    public override fun onStop(){
        super.onStop()
        //Log cat print out
        Log.i(TAG, "onStop called")

        vStop = findViewById(R.id.stop) as TextView
        stopCounter+=1
        vStop.text = "onStop() calls:" + stopCounter.toString()
        with(getPreferences(Context.MODE_PRIVATE).edit()){
            putInt("createCounter",createCounter)
            putInt("startCounter",startCounter)
            putInt("resumeCounter",resumeCounter)
            putInt("pauseCounter",pauseCounter)
            putInt("stopCounter",stopCounter)
            putInt("restartCounter",restartCounter)
            putInt("destroyCounter",destroyCounter)

            // prefer apply() but this is a simple app
            Log.i(TAG, "Saved the Counters")
            apply()
        }


        // Store values between app instances
        // we need an editor, we are writing.

    }
    // TODO: implement 3 missing lifecycle callback methods with Logging and
    // TODO: increment the counter variables' values when their corresponding lifecycle methods are called and log the info

    // Note:  if you want to use a resource as a string you must do the following
    //  getResources().getString(R.string.stringname)   returns a String.

    public override fun onSaveInstanceState(savedInstanceState: Bundle) {
        //TODO:  save state information with a collection of key-value pairs & save all  count variables

        /*
        //save my 7 counters
        savedInstanceState?.run {
            putInt("createCounter", createCounter)
            putInt("startCounter", startCounter)
            putInt("resumeCounter", resumeCounter)
            putInt("pauseCounter", pauseCounter)
            putInt("restartCounter", restartCounter)
            putInt("stopCounter", stopCounter)
            putInt("restartCounter", restartCounter)
            putInt("destroyCounter", destroyCounter)
        }*/
        //call superclass so it will save view hierarchy
        super.onSaveInstanceState(savedInstanceState)
    }
    //Runs at onStart() and onResume()
    /*public override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState != null){
            Log.i(TAG,"works")
            with(savedInstanceState){
                createCounter = getInt("createCounter")
                startCounter = getInt("startCounter")
                resumeCounter = getInt("resumeCounter")
                pauseCounter = getInt("pauseCounter")
                stopCounter = getInt("stopCounter")
                restartCounter = getInt("restartCounter")
                destroyCounter = getInt("destroyCounter")
            }
        }else{
            Log.i(TAG,"")
            createCounter = 0
            startCounter = 0
            resumeCounter = 0
            pauseCounter = 0
            stopCounter = 0
            restartCounter = 0
            destroyCounter = 0
        }
    }*/
    fun saveCounterInfo(view:View){
        createCounter = (findViewById(R.id.create) as TextView).text.toString().toInt()
        startCounter = (findViewById(R.id.start) as TextView).text.toString().toInt()
        resumeCounter = (findViewById(R.id.resume) as TextView).text.toString().toInt()
        pauseCounter = (findViewById(R.id.pause) as TextView).text.toString().toInt()
        stopCounter = (findViewById(R.id.stop) as TextView).text.toString().toInt()
        restartCounter = (findViewById(R.id.restart) as TextView).text.toString().toInt()
        destroyCounter = (findViewById(R.id.destroy) as TextView).text.toString().toInt()

        // Store values between app instances
        // we need an editor, we are writing.

        with(getPreferences(Context.MODE_PRIVATE).edit()){
            putInt("createCount",createCounter)
            putInt("startCount",startCounter)
            putInt("resumeCount",resumeCounter)
            putInt("pauseCount",pauseCounter)
            putInt("stopCount",stopCounter)
            putInt("restartCount",restartCounter)
            putInt("desCount",destroyCounter)

            // prefer apply() but this is a simple app
            commit()
        }
    }

    fun launchActivityTwo(view: View) {
        startActivity(Intent(this, ActivityTwo::class.java))
    }

    // singleton object in kotlin
    // companion object
    companion object {
        // string for logcat
        private val TAG = "Lab-ActivityOne"
    }


}
